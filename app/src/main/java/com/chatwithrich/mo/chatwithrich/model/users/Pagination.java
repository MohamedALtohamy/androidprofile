package com.chatwithrich.mo.chatwithrich.model.users;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Pagination{
  @SerializedName("per_page")
  @Expose
  private Integer per_page;
  @SerializedName("total")
  @Expose
  private Integer total;
  @SerializedName("has_more_pages")
  @Expose
  private Boolean has_more_pages;
  @SerializedName("last_page")
  @Expose
  private Integer last_page;
  @SerializedName("first_item")
  @Expose
  private Integer first_item;
  @SerializedName("last_item")
  @Expose
  private Integer last_item;
  @SerializedName("next_page_url")
  @Expose
  private String next_page_url;
  @SerializedName("prev_page_url")
  @Expose
  private Object prev_page_url;
  @SerializedName("current_page")
  @Expose
  private Integer current_page;
  public void setPer_page(Integer per_page){
   this.per_page=per_page;
  }
  public Integer getPer_page(){
   return per_page;
  }
  public void setTotal(Integer total){
   this.total=total;
  }
  public Integer getTotal(){
   return total;
  }
  public void setHas_more_pages(Boolean has_more_pages){
   this.has_more_pages=has_more_pages;
  }
  public Boolean getHas_more_pages(){
   return has_more_pages;
  }
  public void setLast_page(Integer last_page){
   this.last_page=last_page;
  }
  public Integer getLast_page(){
   return last_page;
  }
  public void setFirst_item(Integer first_item){
   this.first_item=first_item;
  }
  public Integer getFirst_item(){
   return first_item;
  }
  public void setLast_item(Integer last_item){
   this.last_item=last_item;
  }
  public Integer getLast_item(){
   return last_item;
  }
  public void setNext_page_url(String next_page_url){
   this.next_page_url=next_page_url;
  }
  public String getNext_page_url(){
   return next_page_url;
  }
  public void setPrev_page_url(Object prev_page_url){
   this.prev_page_url=prev_page_url;
  }
  public Object getPrev_page_url(){
   return prev_page_url;
  }
  public void setCurrent_page(Integer current_page){
   this.current_page=current_page;
  }
  public Integer getCurrent_page(){
   return current_page;
  }
}