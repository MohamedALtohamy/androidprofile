package com.chatwithrich.mo.chatwithrich.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.chatwithrich.mo.chatwithrich.R;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.sdsmdg.tastytoast.TastyToast;

/**
 * Created by mo on 7/14/2018.
 */

public class SocialChat {

    public static void whatsApp(Activity activity, String number) {
        number=number.replace("c","");
        try {
            if (isAppAvailable(activity, "com.whatsapp")) {
                number = number.replace(" ", "").replace("+", "");

                Intent sendIntent = new Intent("android.intent.action.MAIN");
                sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
                sendIntent.putExtra("jid", PhoneNumberUtils.stripSeparators(number) + "@s.whatsapp.net");
                activity.startActivity(sendIntent);
            }


        } catch (Exception e) {
            // Log.e(TAG, "ERROR_OPEN_MESSANGER"+e.toString());
        }

    }

    public static void FB(Activity activity, String facebookId) {
        facebookId=facebookId.replace(" ","");
        facebookId=facebookId.toLowerCase();

        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://m.me/" + facebookId));
        activity.startActivity(intent);
    }

    public static void Telegrame(Activity activity, String telegramId) {
        if(telegramId.startsWith("@")){
            telegramId=telegramId.replaceFirst("@","");
        }
        if (isAppAvailable(activity, "org.telegram.messenger")) {
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://telegram.me/" + telegramId));
            intent.setPackage("org.telegram.messenger");
            activity.startActivity(intent);
        }

    }

    public static void skype(Activity activity, String skypename) {
        try {
            //Intent sky = new Intent("android.intent.action.CALL_PRIVILEGED");
            //the above line tries to create an intent for which the skype app doesn't supply public api
/*
            Intent sky = new Intent("android.intent.action.VIEW");
            sky.setData(Uri.parse("skype:" + skypename));
            activity.startActivity(sky);
*/
            Intent skype_intent = new Intent("android.intent.action.VIEW");
            skype_intent.setClassName("com.skype.raider", "com.skype.raider.Main");
            skype_intent.setData(Uri.parse("skype:" + skypename + "?call&video=true"));
            activity.startActivity(skype_intent);
        } catch (ActivityNotFoundException e) {
            Log.e("SKYPE CALL", "Skype failed", e);
        }

    }

    public static void instgram(Activity activity, String instgram) {
        Uri uri = Uri.parse("http://instagram.com/_u/" + instgram);
        Intent likeIng = new Intent(Intent.ACTION_VIEW, uri);

        likeIng.setPackage("com.instagram.android");

        try {
            activity.startActivity(likeIng);
        } catch (ActivityNotFoundException e) {
            activity.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://instagram.com" + instgram)));
        }

    }
    public static void email(Activity activity, String email) {
        activity.startActivity(new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"+email)));

    }


    public static boolean isAppAvailable(Activity context, String appName) {
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (Exception e) {
            TastyToast.makeText(context, context.getString(R.string.app_not_installed), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            return false;
        }
    }

}
