package com.chatwithrich.mo.chatwithrich.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chatwithrich.mo.chatwithrich.R;
import com.chatwithrich.mo.chatwithrich.dialog.DialogInfoApp;
import com.chatwithrich.mo.chatwithrich.interfaces.HandleRetrofitResp;
import com.chatwithrich.mo.chatwithrich.model.profile.Data;
import com.chatwithrich.mo.chatwithrich.model.profile.Profile;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.HandelCalls;
import com.chatwithrich.mo.chatwithrich.utlitites.DataEnum;
import com.chatwithrich.mo.chatwithrich.utlitites.PrefsUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.HashMap;
import java.util.List;

public class LoginActivity extends AppCompatActivity implements Validator.ValidationListener, HandleRetrofitResp {
//https://www.flaticon.com

    private Toolbar mToolbar;

    @Email
    private EditText mLoginEmail;
    @NotEmpty
    private EditText mLoginPassword;

    private Button mLogin_btn;

    private Validator validator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initToolBar();

        initView();
        initValidate();
        mLoginEmail.setText("moh@gmail.com");
       mLoginPassword.setText("123123");
        //


    }

    private void initValidate() {
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initView() {
        mLoginEmail = (EditText) findViewById(R.id.login_email);
        mLoginPassword = (EditText) findViewById(R.id.login_password);
        mLogin_btn = (Button) findViewById(R.id.login_btn);
        findViewById(R.id.tvDescreption).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                DialogInfoApp.showDialog(LoginActivity.this, "");

            }
        });

        mLogin_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validator.validate();


            }
        });
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.login_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Login");
    }


    private void loginUser(String email, String password) {


    }

    @Override
    public void onValidationSucceeded() {
        String email = mLoginEmail.getText().toString();
        String password = mLoginPassword.getText().toString();

       // loginUser(email, password);

        HashMap<String, String> meMap=new HashMap<String, String>();
        meMap.put("email",email);
        meMap.put("password",password);

        HandelCalls.getInstance(LoginActivity.this).call(DataEnum.LoginApiCall.name(),meMap,true,this);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        Profile profile= (Profile) o;
        if(profile.getStatus().getStatus()){
            PrefsUtil.with(this).add(DataEnum.shProfile.name(),profile.getData()).apply();
            Log.e("token",profile.getData().getToken());
            PrefsUtil.with(this).add(DataEnum.AuthToken.name(),profile.getData().getToken()).apply();

            Data s= (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(),Data.class);
            Log.e("my nmae",profile.getData().getFull_name());
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        }else{
            TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
        }

    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }
}
