package com.chatwithrich.mo.chatwithrich.adapter;

import android.app.Activity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chatwithrich.mo.chatwithrich.R;
import com.chatwithrich.mo.chatwithrich.activity.RegisterActivity;
import com.chatwithrich.mo.chatwithrich.dialog.DialogInfoApp;
import com.chatwithrich.mo.chatwithrich.dialog.SocialChat;
import com.chatwithrich.mo.chatwithrich.model.users.Data;
import com.chatwithrich.mo.chatwithrich.utlitites.HelpMe;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * Created by lenovo on 5/3/2016.
 */
public class AdapterUsers extends RecyclerView.Adapter<AdapterUsers.ViewHolder> {


    public List<Data> data;

    private Activity mContext;


    public AdapterUsers(List<Data> data, Activity mContext) {
        this.data = data;
        this.mContext = mContext;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_result, parent, false);
        //  ButterKnife.bind(mContext, view);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // holder.tvDoctoreName.setText(data.get(position).getFirstName() + " " + data.get(position).getLastName());
        Data profile = data.get(position);
        holder.tvTitle.setText(isUnkonwn(profile.getStatus_note()));
        holder.tvCity.setText(isUnkonwn(profile.getCountry_code()));
        holder.tvGender.setText(isUnkonwn(profile.getGender()));
        String level = HelpMe.getInstance(mContext).initListLevels().get(Integer.parseInt(data.get(position).getLevel()));
        holder.tvLevel.setText(level);
        Picasso.with(mContext)
                .load(isUnkonwn(profile.getAvatar()))
                .placeholder(R.drawable.default_avatar).into(holder.img);
        if (profile.getFacebook_acc().equals("")) {
            //  holder.imgFB.setImageResource(R.drawable.ic_messenger_not_active);
            holder.imgFB.setVisibility(View.GONE);
        } else {
            holder.imgFB.setVisibility(View.VISIBLE);
        }
        if (profile.getWhatsapp_acc().equals("")) {
            // holder.imgWhats.setImageResource(R.drawable.ic_whatsappun);
            holder.imgWhats.setVisibility(View.GONE);
        } else {
            holder.imgWhats.setVisibility(View.VISIBLE);
        }
        if (profile.getTelegram_acc().equals("")) {
            //holder.imgTelegram.setImageResource(R.drawable.ic_telegramun);
            holder.imgTelegram.setVisibility(View.GONE);
        } else {
            holder.imgTelegram.setVisibility(View.VISIBLE);
        }
        if (profile.getInstagram_acc().equals("")) {
            //  holder.imginstgram.setImageResource(R.drawable.ic_telegramun);
            holder.imginstgram.setVisibility(View.GONE);
        } else {
            holder.imginstgram.setVisibility(View.VISIBLE);
        }
    }

    private String isUnkonwn(String value) {
        if (value.equals("")) {
            return mContext.getString(R.string.unknown);
        } else {
            return value;
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void add(String string) {
        insert(string, data.size());
    }

    public void insert(String string, int position) {
        //data.add(position, string);
        notifyItemInserted(position);
    }

    public void remove(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        int size = data.size();
        data.clear();
        notifyItemRangeRemoved(0, size);
    }

    public void addAll(List<Data> items) {
        int startIndex = data.size();
        data.addAll(items);
        notifyItemRangeInserted(startIndex, items.size());
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        ImageView img;
        TextView tvTitle;

        TextView tvCity;
        ImageView imggender;
        TextView tvGender;
        TextView tvLevel;
        ImageView imgFB;
        ImageView imgTelegram;
        ImageView imgWhats;
        ImageView imgEmail;
        ImageView imginstgram;
        CardView cardView;

        public ViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            img = itemView.findViewById(R.id.img);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvTitle.setOnClickListener(this);
            tvCity = itemView.findViewById(R.id.tvCity);
            tvGender = itemView.findViewById(R.id.tvGender);
            tvLevel = itemView.findViewById(R.id.tvLevel);

            imgFB = itemView.findViewById(R.id.imgFB);
            imgTelegram = itemView.findViewById(R.id.imgTelegram);
            imgWhats = itemView.findViewById(R.id.imgWhats);
            imgEmail = itemView.findViewById(R.id.imgEmail);
            imginstgram = itemView.findViewById(R.id.imginstgram);
            imgFB.setOnClickListener(this);
            imgTelegram.setOnClickListener(this);
            imgWhats.setOnClickListener(this);
            imgEmail.setOnClickListener(this);
            imginstgram.setOnClickListener(this);


        }


        @Override
        public void onClick(View v) {

            switch (v.getId()) {
                case R.id.tvTitle:

                    break;
                case R.id.imgFB:
                    SocialChat.FB(mContext, data.get(getAdapterPosition()).getFacebook_acc());
                    break;
                case R.id.imgTelegram:
                    SocialChat.Telegrame(mContext, data.get(getAdapterPosition()).getTelegram_acc());
                    break;
                case R.id.imgWhats:
                    SocialChat.whatsApp(mContext, data.get(getAdapterPosition()).getWhatsapp_acc());
                    break;
                case R.id.imgEmail:
                    SocialChat.email(mContext, data.get(getAdapterPosition()).getEmail());

                    break;
                case R.id.imginstgram:
                    SocialChat.instgram(mContext, data.get(getAdapterPosition()).getInstagram_acc());
                    break;

            }
        }

    }
}
