package com.chatwithrich.mo.chatwithrich.model.profile;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Profile {
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("status")
  @Expose
  private Status status;
  public void setData(Data data){
   this.data=data;
  }
  public Data getData(){
   return data;
  }
  public void setStatus(Status status){
   this.status=status;
  }
  public Status getStatus(){
   return status;
  }
}