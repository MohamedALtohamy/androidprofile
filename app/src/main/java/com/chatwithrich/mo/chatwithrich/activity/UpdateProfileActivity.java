package com.chatwithrich.mo.chatwithrich.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Space;
import android.widget.TextView;
import android.widget.Toast;

import com.chatwithrich.mo.chatwithrich.R;
import com.chatwithrich.mo.chatwithrich.dialog.DialogInfoApp;
import com.chatwithrich.mo.chatwithrich.interfaces.HandleRetrofitResp;
import com.chatwithrich.mo.chatwithrich.model.profile.Data;
import com.chatwithrich.mo.chatwithrich.model.profile.Profile;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.HandelCalls;
import com.chatwithrich.mo.chatwithrich.utlitites.DataEnum;
import com.chatwithrich.mo.chatwithrich.utlitites.PrefsUtil;
import com.greysonparrelli.permiso.Permiso;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.sdsmdg.tastytoast.TastyToast;
import com.squareup.picasso.Picasso;
import com.van.fanyu.library.Compresser;

import java.io.File;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class UpdateProfileActivity extends AppCompatActivity implements HandleRetrofitResp, com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener {

    //region fields
    @BindView(R.id.settings_image)
    CircleImageView settingsImage;
    @BindView(R.id.sp)
    Space sp;
    @BindView(R.id.switch_location)
    SwitchCompat switchLocation;
    @BindView(R.id.status)
    EditText status;
    @BindView(R.id.setting_update_btn)
    Button settingUpdateBtn;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.tv_country)
    TextView tvCountry;
    @BindView(R.id.tv_birthdate)
    TextView tvBirthdate;
    @BindView(R.id.tv_male)
    TextView tvMale;
    @BindView(R.id.tv_female)
    TextView tvFemale;
    @BindView(R.id.rel_social)
    RelativeLayout relSocial;

    Toolbar mToolbar;

    //endregion
    //gender 0 not selected 1 male 2 female
    private int gender = 0;
    private Calendar calendar;
    private Uri ImageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        calendar = Calendar.getInstance();
        Permiso.getInstance().setActivity(this);
        initToolBar();
        initValues();
        initGender();


        // showCountry(this);

        //  DialogInfoApp.showDialogSearch(this);

        //avatar


        // HandelCalls.getInstance(UpdateProfileActivity.this).call(DataEnum.updateProfile.name(), meMap, true, this);


    }

    private void initValues() {


        Data profile = (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(), Data.class);
        Picasso.with(this)
                .load(isUnkonwn(profile.getAvatar_link()))
                .placeholder(R.drawable.default_avatar).into(settingsImage);
        email.setText(profile.getEmail());
        setTextV(tvCountry, profile.getCountry_code());
        setTextV(tvBirthdate, profile.getDate_of_birth());
        gender = Integer.parseInt(profile.getGender());
        if (!profile.getStatus_note().equals("")) {
            status.setText(profile.getStatus_note());
        }

    }

    private void setTextV(TextView tv, String value) {
        if (!value.equals("")) {
            tv.setText(value);
        }
    }

    private String isUnkonwn(String value) {
        if (value.equals("")) {
            return getString(R.string.unknown);
        } else {
            return value;
        }
    }


    private void initGender() {
        if (gender == 1) {
            tvMale.setBackground(getResources().getDrawable(R.drawable.active_gender));
            tvFemale.setBackground(getResources().getDrawable(R.drawable.gender));
        } else if (gender == 2) {
            tvMale.setBackground(getResources().getDrawable(R.drawable.gender));
            tvFemale.setBackground(getResources().getDrawable(R.drawable.active_gender));
        } else {
            tvMale.setBackground(getResources().getDrawable(R.drawable.gender));
            tvFemale.setBackground(getResources().getDrawable(R.drawable.gender));
        }
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.update_profile_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.update_profile));
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @OnClick({R.id.settings_image, R.id.setting_update_btn})
    public void onViewClickedImage(View view) {
        switch (view.getId()) {
            case R.id.settings_image:
                Permission();
                break;
            case R.id.setting_update_btn:
                updateProfile();
                break;
        }
    }

    private void updateProfile() {


        if (ImageUri == null) {
            HashMap<String, String> meMap = new HashMap<String, String>();
            meMap.put("country_code", tvCountry.getText().toString());
            //1 male 2 female
            meMap.put("gender", gender + "");
            // meMap.put("level","Red");
            meMap.put("date_of_birth", tvBirthdate.getText().toString());
            // meMap.put("long_point", "Red");
            //  meMap.put("lat_point", "Red");
            meMap.put("status_note", status.getText().toString());
            // meMap.put("facebook_acc", "Red");
            // meMap.put("whatsapp_acc", "Red");
            // meMap.put("telegram_acc", "Red");
            //   meMap.put("instagram_acc", "Red");
            meMap.put("_method", "put");
            HandelCalls.getInstance(UpdateProfileActivity.this).call(DataEnum.updateProfile.name(), meMap, true, this);
        } else {
            HashMap<String, RequestBody> meMap = new HashMap<String, RequestBody>();
            meMap.put("country_code", createPartFromString(tvCountry.getText().toString()));
            //1 male 2 female
            meMap.put("gender", createPartFromString(gender + ""));
            // meMap.put("level","Red");
            meMap.put("date_of_birth", createPartFromString(tvBirthdate.getText().toString()));
            // meMap.put("long_point", "Red");
            //  meMap.put("lat_point", "Red");
            meMap.put("status_note", createPartFromString(status.getText().toString()));
            // meMap.put("facebook_acc", "Red");
            // meMap.put("whatsapp_acc", "Red");
            // meMap.put("telegram_acc", "Red");
            //   meMap.put("instagram_acc", "Red");
            meMap.put("_method", createPartFromString("put"));
            HandelCalls.getInstance(UpdateProfileActivity.this).callWithImage(DataEnum.updateProfile.name(), meMap, prepareFilePart("avatar", ImageUri), true, this);
        }


    }

    @NonNull
    private RequestBody createPartFromString(String description) {
        return RequestBody.create(okhttp3.MultipartBody.FORM, description);
    }

    @NonNull
    private MultipartBody.Part prepareFilePart(String partName, Uri fileUri) {
        File file = new File(fileUri.getPath());
        // create RequestBody instance from file
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        // MultipartBody.Part is used to send also the actual file name
        return MultipartBody.Part.createFormData(partName, file.getName(), requestFile);
    }

    @OnClick({R.id.tv_country, R.id.tv_birthdate, R.id.tv_male, R.id.tv_female, R.id.rel_social})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_country:
                showCountry(UpdateProfileActivity.this);
                break;
            case R.id.tv_birthdate:
                showCalendar();
                break;
            case R.id.tv_male:
                gender = 1;
                initGender();
                break;
            case R.id.tv_female:
                gender = 2;
                initGender();
                break;
            case R.id.rel_social:
                //  DialogInfoApp.showDialogSocial(this);
                break;
        }
    }

    public void showCountry(AppCompatActivity activity) {
        CountryPicker countryPicker =
                new CountryPicker.Builder().with(activity)
                        .listener(new OnCountryPickerListener() {
                            @Override
                            public void onSelectCountry(Country country) {
                                //DO something here
                                Log.e("country", country.getName());
                                tvCountry.setText(country.getName());
                            }
                        })
                        .build();


        countryPicker.showDialog(activity.getSupportFragmentManager());
        //  Country country = countryPicker.getCountryFromSIM();
        //  Log.e("country",country.getName()+" "+country.getCode() );
    }

    private void showCalendar() {
        com.wdullaer.materialdatetimepicker.date.DatePickerDialog dpd = com.wdullaer.materialdatetimepicker.date.DatePickerDialog.newInstance(
                UpdateProfileActivity.this,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        );
        //  dpd.setThemeDark(true);

        dpd.setMaxDate(calendar);
        dpd.vibrate(true);
        dpd.dismissOnPause(true);
        // dpd.showYearPickerFirst(true);
        dpd.setVersion(com.wdullaer.materialdatetimepicker.date.DatePickerDialog.Version.VERSION_2);
        dpd.setAccentColor(getResources().getColor(R.color.goldenColor));
        // dpd.setVersion(showVersion2.isChecked() ? DatePickerDialog.Version.VERSION_2 : DatePickerDialog.Version.VERSION_1);
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    @Override
    public void onResume() {
        super.onResume();
        Permiso.getInstance().setActivity(this);

    }


    private void Permission() {
        Permiso.getInstance().requestPermissions(new Permiso.IOnPermissionResult() {
            @Override
            public void onPermissionResult(Permiso.ResultSet resultSet) {
                if (resultSet.isPermissionGranted(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && resultSet.isPermissionGranted(Manifest.permission.CAMERA)) {
                    //Log.e("yss", "agree");

                    picImage();

                } else if (resultSet.isPermissionPermanentlyDenied(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        && resultSet.isPermissionPermanentlyDenied(Manifest.permission.CAMERA)) {
                    Toast.makeText(getApplicationContext(), getString(R.string.permstion_need), Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(getApplicationContext(), getString(R.string.permstion_need), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onRationaleRequested(Permiso.IOnRationaleProvided callback, String... permissions) {
                Permiso.getInstance().showRationaleInDialog(getString(R.string.title_dialog_permtion), getString(R.string.permtion_dialog_reason), null, callback);
            }

        }, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Permiso.getInstance().onRequestPermissionResult(requestCode, permissions, grantResults);
    }

    private void picImage() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(UpdateProfileActivity.this)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {
                        Log.d("ted", "uri: " + uri);
                        Log.d("ted", "uri.getPath(): " + uri.getPath());
                        compressedFile(uri);
                        ImageUri = uri;

                    }
                })
                //.setPeekHeight(getResources().getDisplayMetrics().heightPixels/2)
                .setPeekHeight(1200)
                .setTitle(getString(R.string.choose_image))
                .create();

        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    private void compressedFile(final Uri uri) {
        // imageUri=uri;
        try {
            Compresser compresser = new Compresser(50, uri.getPath());
            compresser.doCompress(new Compresser.CompleteListener() {
                @Override
                public void onSuccess(String newPath) {
                    //Log.e("from", "comprees");
                    settingsImage.setImageURI(Uri.parse(newPath));
                    // imageUri = Uri.parse(newPath);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        Profile profile = (Profile) o;
        if (profile.getStatus().getStatus()) {
            PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();
            // Data s= (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(),Data.class);
            // Log.e("my nmae",profile.getData().getFull_name());
            // startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
            TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.SUCCESS);
        } else {
            TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
        }
    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }

    @Override
    public void onDateSet(com.wdullaer.materialdatetimepicker.date.DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        calendar.set(year, monthOfYear, dayOfMonth);

        java.text.SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        tvBirthdate.setText(simpleDateFormat.format(calendar.getTime()));
    }
}
