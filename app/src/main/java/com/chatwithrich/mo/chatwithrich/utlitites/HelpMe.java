package com.chatwithrich.mo.chatwithrich.utlitites;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.RectF;
import android.util.Log;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.ConnectException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Mina Shaker on 27-Mar-18.
 */

public class HelpMe {

    // static Uri.Builder builder;
    public  final ArrayList<String> mStrings = new ArrayList<>();
    private static Context context;
    private static HelpMe instance = null;


    private Gson gson;
//  public   Socket mSocket;

    public static HelpMe getInstance(Context context) {


        HelpMe.context = context;

        if (instance == null) {
            instance = new HelpMe();
        }
        return instance;
    }
    public ArrayList<String> initListLevels() {
       if(mStrings.size()==0){
           mStrings.add("A $50,000");
           mStrings.add("B $40,000");
           mStrings.add("C $30,000");
           mStrings.add("D $20,000");
           mStrings.add("E $10,000");
           mStrings.add("F $9,000");
           mStrings.add("G $8,000");
           mStrings.add("H $7,000");
           mStrings.add("I $6,000");
           mStrings.add("J $5,000");
           mStrings.add("K $4,000");
           mStrings.add("L $3,000");
           mStrings.add("M $2,000");
           mStrings.add("N $1,000");
           mStrings.add("O $9,00");
           mStrings.add("P $8,00");
           mStrings.add("Q $7,00");
           mStrings.add("R $6,00");
           mStrings.add("S $5,00");
           mStrings.add("T $4,00");
           mStrings.add("U $3,00");
           mStrings.add("V $2,00");
           mStrings.add("W $1,00");
           mStrings.add("X $50");
           mStrings.add("Y $25");
           mStrings.add("Z $10");
       }
       return mStrings;
        /*
        Gson gson = new Gson();
        Log.e("jsone",gson.toJson(mStrings)) ;
        String js="[\"A $50,000\",\"B $40,000\",\"C $30,000\",\"D $20,000\",\"E $10,000\",\"F $9,000\",\"G $8,000\",\"H $7,000\",\"I $6,000\",\"J $5,000\",\"K $4,000\",\"L $3,000\",\"M $2,000\",\"N $1,000\",\"O $9,00\",\"P $8,00\",\"Q $7,00\",\"R $6,00\",\"S $5,00\",\"T $4,00\",\"U $3,00\",\"V $2,00\",\"W $1,00\",\"X $50\",\"Y $25\",\"Z $10\"]";
     ArrayList<String> c = gson.fromJson(js,ArrayList.class);
     Log.e("fromobj",c.get(6));
     */
    }

    public boolean isTablet() {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public Object parseJsonToObject(String response, Class<?> modelClass) {
        if (gson == null) {
            gson = new GsonBuilder().serializeNulls().create();
        }

        try {
            return gson.fromJson(response, modelClass);
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    /**
     * Serializes the specified object into its equivalent Json representation.
     *
     * @param object The object which Json will represent.
     * @return Json representation of src.
     */
    public String parseObjectToJson(Object object) {
        if (gson == null) {
            gson = new GsonBuilder().serializeNulls().create();
        }
        return gson.toJson(object);
    }


    public boolean isAppInstalled(String packageName) {
        try {
            //boolean whatsappFound = AndroidHelper.isAppInstalled(context, "com.whatsapp");
            context.getPackageManager().getApplicationInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }




    public void initLang(String lang) {

        //  String lang = SharedPrefUtil.getInstance(getApplicationContext()).read("settingLangName", "en");
        //  String lang = "ar";
        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config,
                context.getResources().getDisplayMetrics());
    }


    public void hideKeyBoard(Activity act) {
        act.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
    }

    public void spliteForLog(String veryLongString) {
        int maxLogStringSize = 1000;
        for (int i = 0; i <= veryLongString.length() / maxLogStringSize; i++) {
            int start = i * maxLogStringSize;
            int end = (i + 1) * maxLogStringSize;
            end = end > veryLongString.length() ? veryLongString.length() : end;
            Log.v("spletres", veryLongString.substring(start, end));
        }
    }

    //======================================================//


    public void hidekeyboard(Activity act) {
        act.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        InputMethodManager imm = (InputMethodManager) act.getSystemService(Context.INPUT_METHOD_SERVICE);
    }







    public Bitmap scaleCenterCrop(Bitmap source, int newHeight, int newWidth) {
        int sourceWidth = source.getWidth();
        int sourceHeight = source.getHeight();

        // Compute the scaling factors to fit the new height and width, respectively.
        // To cover the final image, the final scaling will be the bigger
        // of these two.
        float xScale = (float) newWidth / sourceWidth;
        float yScale = (float) newHeight / sourceHeight;
        float scale = Math.max(xScale, yScale);

        // Now get the size of the source bitmap when scaled
        float scaledWidth = scale * sourceWidth;
        float scaledHeight = scale * sourceHeight;

        // Let's find out the upper left coordinates if the scaled bitmap
        // should be centered in the new size give by the parameters
        float left = (newWidth - scaledWidth) / 2;
        float top = (newHeight - scaledHeight) / 2;

        // The target rectangle for the new, scaled version of the source bitmap will now
        // be
        RectF targetRect = new RectF(left, top, left + scaledWidth, top + scaledHeight);

        // Finally, we create a new bitmap of the specified size and draw our new,
        // scaled bitmap onto it.
        Bitmap dest = Bitmap.createBitmap(newWidth, newHeight, source.getConfig());
        Canvas canvas = new Canvas(dest);
        canvas.drawBitmap(source, null, targetRect, null);

        return dest;
    }





    public void retrofironFailure(Throwable t) {

        if (t instanceof ConnectException) {
           // TastyToast.makeText(context, t.getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
        } else {
         //   TastyToast.makeText(context, t.getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            Log.e("errrr", t.getMessage());
        }
    }

    public void retrofirOnNotTwoHundred(int x) {
        Log.e("codeis", x + "");
        if (x == 204) {
            // TastyToast.makeText(context, context.getString(R.string.no_content), TastyToast.LENGTH_SHORT, TastyToast.ERROR);
        } else if (x == 400) {
            //TastyToast.makeText(context, context.getString(R.string.no_data), TastyToast.LENGTH_SHORT, TastyToast.ERROR);
        }


    }

    /*================================================================================*/




}
