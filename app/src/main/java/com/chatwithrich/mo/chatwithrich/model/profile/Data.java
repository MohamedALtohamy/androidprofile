package com.chatwithrich.mo.chatwithrich.model.profile;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Data{

        @SerializedName("status_note")
        @Expose
        private String status_note;
        @SerializedName("instagram_acc")
        @Expose
        private String instagram_acc;
        @SerializedName("gender")
        @Expose
        private String gender;
        @SerializedName("telegram_acc")
        @Expose
        private String telegram_acc;
        @SerializedName("level")
        @Expose
        private String level;
        @SerializedName("date_of_birth")
        @Expose
        private String date_of_birth;
        @SerializedName("date_of_birth_readable")
        @Expose
        private String date_of_birth_readable;
        @SerializedName("long_point")
        @Expose
        private String long_point;
        @SerializedName("avatar_link")
        @Expose
        private String avatar_link;
        @SerializedName("avatar")
        @Expose
        private String avatar;
        @SerializedName("lat_point")
        @Expose
        private String lat_point;
        @SerializedName("skype_acc")
        @Expose
        private String skype_acc;
        @SerializedName("country_code")
        @Expose
        private String country_code;
        @SerializedName("full_name")
        @Expose
        private String full_name;
        @SerializedName("profile_id")
        @Expose
        private String profile_id;
        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("facebook_acc")
        @Expose
        private String facebook_acc;
        @SerializedName("whatsapp_acc")
        @Expose
        private String whatsapp_acc;
    @SerializedName("token")
    @Expose
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public void setStatus_note(String status_note){
            this.status_note=status_note;
        }
        public String getStatus_note(){
            return status_note;
        }
        public void setInstagram_acc(String instagram_acc){
            this.instagram_acc=instagram_acc;
        }
        public String getInstagram_acc(){
            return instagram_acc;
        }
        public void setGender(String gender){
            this.gender=gender;
        }
        public String getGender(){
            return gender;
        }
        public void setTelegram_acc(String telegram_acc){
            this.telegram_acc=telegram_acc;
        }
        public String getTelegram_acc(){
            return telegram_acc;
        }
        public void setLevel(String level){
            this.level=level;
        }
        public String getLevel(){
            return level;
        }
        public void setDate_of_birth(String date_of_birth){
            this.date_of_birth=date_of_birth;
        }
        public String getDate_of_birth(){
            return date_of_birth;
        }
        public void setDate_of_birth_readable(String date_of_birth_readable){
            this.date_of_birth_readable=date_of_birth_readable;
        }
        public String getDate_of_birth_readable(){
            return date_of_birth_readable;
        }
        public void setLong_point(String long_point){
            this.long_point=long_point;
        }
        public String getLong_point(){
            return long_point;
        }
        public void setAvatar_link(String avatar_link){
            this.avatar_link=avatar_link;
        }
        public String getAvatar_link(){
            return avatar_link;
        }
        public void setAvatar(String avatar){
            this.avatar=avatar;
        }
        public String getAvatar(){
            return avatar;
        }
        public void setLat_point(String lat_point){
            this.lat_point=lat_point;
        }
        public String getLat_point(){
            return lat_point;
        }
        public void setSkype_acc(String skype_acc){
            this.skype_acc=skype_acc;
        }
        public String getSkype_acc(){
            return skype_acc;
        }
        public void setCountry_code(String country_code){
            this.country_code=country_code;
        }
        public String getCountry_code(){
            return country_code;
        }
        public void setFull_name(String full_name){
            this.full_name=full_name;
        }
        public String getFull_name(){
            return full_name;
        }
        public void setProfile_id(String profile_id){
            this.profile_id=profile_id;
        }
        public String getProfile_id(){
            return profile_id;
        }
        public void setId(String  id){
            this.id=id;
        }
        public String getId(){
            return id;
        }
        public void setEmail(String email){
            this.email=email;
        }
        public String getEmail(){
            return email;
        }
        public void setFacebook_acc(String facebook_acc){
            this.facebook_acc=facebook_acc;
        }
        public String getFacebook_acc(){
            return facebook_acc;
        }
        public void setWhatsapp_acc(String whatsapp_acc){
            this.whatsapp_acc=whatsapp_acc;
        }
        public String getWhatsapp_acc(){
            return whatsapp_acc;

}}