package com.chatwithrich.mo.chatwithrich;

import android.app.Application;


import com.squareup.picasso.OkHttpDownloader;
import com.squareup.picasso.Picasso;

/**
 * Created
 */

public class ChatWithRitch extends Application {



    @Override
    public void onCreate() {
        super.onCreate();



        /* Picasso */

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttpDownloader(this, Integer.MAX_VALUE));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(true);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);





    }


}
