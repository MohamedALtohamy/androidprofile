package com.chatwithrich.mo.chatwithrich.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chatwithrich.mo.chatwithrich.R;
import com.chatwithrich.mo.chatwithrich.activity.HomeActivity;
import com.chatwithrich.mo.chatwithrich.activity.UpdateProfileActivity;
import com.chatwithrich.mo.chatwithrich.model.profile.Data;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.HandelCalls;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.RestRetrofit;
import com.chatwithrich.mo.chatwithrich.utlitites.DataEnum;
import com.chatwithrich.mo.chatwithrich.utlitites.PrefsUtil;
import com.lamudi.phonefield.PhoneEditText;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import libs.mjn.prettydialog.PrettyDialog;
import libs.mjn.prettydialog.PrettyDialogCallback;

/**
 * Created by mo on 7/14/2018.
 */

public class DialogInfoApp {


    private static DialogInfoApp instance = null;
    private static Context context;

    public static DialogInfoApp getInstance(Context context) {

        DialogInfoApp.context = context;

        if (instance == null) {
            instance = new DialogInfoApp();

        }
        return instance;
    }

    public static void showDialog(Activity activity, String msg) {
        msg = activity.getResources().getString(R.string.info_app);
        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_info);

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        Button dialogButton = (Button) dialog.findViewById(R.id.btn_dialog);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showDialogSocial(final HomeActivity activity) {

        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        View view = View.inflate(context, R.layout.dialog_social_list, null);
       // ButterKnife.bind(context, view);
        dialog.setContentView(view);

        Button dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
        Button btnOk = (Button) dialog.findViewById(R.id.btnOk);
        LinearLayout linear_fb = (LinearLayout) dialog.findViewById(R.id.linear_fb);
        LinearLayout linear_whats = (LinearLayout) dialog.findViewById(R.id.linear_whats);
        LinearLayout linear_instgram = (LinearLayout) dialog.findViewById(R.id.linear_instgram);
        LinearLayout linear_telegram = (LinearLayout) dialog.findViewById(R.id.linear_telegram);
        //
        final EditText edt_fb = (EditText) dialog.findViewById(R.id.edt_fb);
        final EditText edt_telegram = (EditText) dialog.findViewById(R.id.edt_telegram);
        final EditText edt_instgram = (EditText) dialog.findViewById(R.id.edt_instgram);
        final PhoneEditText edt_whats_number = (PhoneEditText) dialog.findViewById(R.id.edt_phone);
      //  final EditText edt_country_code = (EditText) dialog.findViewById(R.id.edt_country_code);
        Data profile = (Data) PrefsUtil.with(activity).get(DataEnum.shProfile.name(), Data.class);
        edt_fb.setText(profile.getFacebook_acc());
        edt_instgram.setText(profile.getInstagram_acc());
        edt_telegram.setText(profile.getTelegram_acc());
        //whats

        CountryPicker countryPicker =
                new CountryPicker.Builder().with(activity)
                        .listener(new OnCountryPickerListener() {
                            @Override
                            public void onSelectCountry(Country country) {
                                //DO something here

                            }
                        })
                        .build();


       // countryPicker.showDialog(activity.getSupportFragmentManager());
        Country country = countryPicker.getCountryFromSIM();
        String strWhats=profile.getWhatsapp_acc();
        if(!profile.getWhatsapp_acc().equals("")){
           edt_whats_number.setPhoneNumber(strWhats);

        }
        else{
            try {
               // edt_country_code.setText(country.getDialCode());
                edt_whats_number.setDefaultCountry(country.getCode());
            }catch (Exception e){

            }

        }
       // country.getDialCode()
        //


        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                HashMap<String, String> meMap=new HashMap<String, String>();
               if(!edt_fb.getText().toString().isEmpty()){
                   meMap.put("facebook_acc",edt_fb.getText().toString());
               }
                if(!edt_telegram.getText().toString().isEmpty()){
                    meMap.put("telegram_acc",edt_telegram.getText().toString());
                }
                if(!edt_instgram.getText().toString().isEmpty()){
                    meMap.put("instagram_acc",edt_instgram.getText().toString());
                }
                if(!edt_fb.getText().toString().isEmpty()){
                    meMap.put("facebook_acc",edt_fb.getText().toString());
                }
                if(edt_whats_number.getEditText().toString().length()>0){
                   if(edt_whats_number.isValid()){
                       meMap.put("whatsapp_acc", edt_whats_number.getPhoneNumber());
                       activity.updateSocilaAccount(meMap);
                       dialog.dismiss();
                   }else{
                       edt_whats_number.setError(activity.getString(R.string.invalid_phone));
                   }

                }
                else{
                    dialog.dismiss();
                    activity.updateSocilaAccount(meMap);

                }







            }
        });
        linear_fb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(context.getString(R.string.fb_help));
            }
        });
        linear_whats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        linear_instgram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        linear_telegram.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openLink(context.getString(R.string.telegram_help));
            }
        });
        dialog_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }

    public void showDialogSearch(Activity activity) {

        final Dialog dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        View view = View.inflate(activity, R.layout.dialog_search_list, null);
        dialog.setContentView(view);


        dialog.show();

    }

    public static void showDialogInfo(Activity activity, String msg) {

        final PrettyDialog dialog = new PrettyDialog(activity);
        dialog.setTitle("PrettyDialog Title")
                .setTitleColor(R.color.goldenColor)
                .setMessage(msg)
                .setIcon(R.drawable.ic_info_outline_black_24dp)
                .addButton(
                        "OK",                    // button text
                        R.color.pdlg_color_white,        // button text color
                        R.color.pdlg_color_green,        // button background color
                        new PrettyDialogCallback() {        // button OnClick listener
                            @Override
                            public void onClick() {
                                dialog.dismiss();
                            }
                        }
                )
                .show();

    }

    public static void showCountry(final AppCompatActivity activity) {
        CountryPicker countryPicker =
                new CountryPicker.Builder().with(activity)
                        .listener(new OnCountryPickerListener() {
                            @Override
                            public void onSelectCountry(Country country) {
                                //DO something here
                                Log.e("country", country.getName());

                            }
                        })
                        .build();


        countryPicker.showDialog(activity.getSupportFragmentManager());
    }

    private void openLink(String link) {

        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        context.startActivity(i);
    }

    @OnClick({R.id.linear_fb, R.id.linear_whats, R.id.linear_telegram, R.id.linear_instgram, R.id.btnOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.linear_fb:
                openLink(context.getString(R.string.fb_help));
                Log.e("fb", "fb");
                break;
            case R.id.linear_whats:
                break;
            case R.id.linear_telegram:
                openLink(context.getString(R.string.telegram_help));
                break;
            case R.id.linear_instgram:
                break;
            case R.id.btnOk:
                break;
        }
    }
}
