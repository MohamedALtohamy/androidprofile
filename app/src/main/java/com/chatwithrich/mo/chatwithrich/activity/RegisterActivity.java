package com.chatwithrich.mo.chatwithrich.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.chatwithrich.mo.chatwithrich.R;

import com.chatwithrich.mo.chatwithrich.interfaces.HandleRetrofitResp;
import com.chatwithrich.mo.chatwithrich.model.profile.Data;
import com.chatwithrich.mo.chatwithrich.model.profile.Profile;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.HandelCalls;
import com.chatwithrich.mo.chatwithrich.utlitites.DataEnum;
import com.chatwithrich.mo.chatwithrich.utlitites.PrefsUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mukesh.countrypicker.Country;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.OnCountryPickerListener;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class RegisterActivity extends AppCompatActivity implements Validator.ValidationListener, HandleRetrofitResp {

    //region field
    @NotEmpty

    @BindView(R.id.reg_password)
    EditText regPassword;

    @BindView(R.id.tvDescreption)
    LinearLayout tvDescreption;
    //@BindView(R.id.SearchableSpinner)
    //gr.escsoft.michaelprimez.searchablespinner.SearchableSpinner SearchableSpinner;
    @Email
    @BindView(R.id.register_email)
    EditText registerEmail;
    @BindView(R.id.ch_temp_account)
    CheckBox chTempAccount;
    @BindView(R.id.reg_create_btn)
    Button regCreateBtn;

    @OnClick(R.id.reg_create_btn)
    public void onViewClicked() {
        validator.validate();
    }

    //endregion
    private Toolbar mToolbar;
   // private SearchableSpinner mSearchableSpinner;
   // private SimpleListAdapter mSimpleArrayListAdapter;
    public static final ArrayList<String> mStrings = new ArrayList<>();

    private Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initToolBar();


        // initSpiner();
        initListValues();
      //  initSpinierLib();
        initValidator();


    }

    private void initValidator() {
         validator = new Validator(this);
        validator.setValidationListener(this);
    }

    private void initToolBar() {
        mToolbar = (Toolbar) findViewById(R.id.register_toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Create Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }



    private void initListValues() {

        mStrings.add("A $50,000");
        mStrings.add("B $40,000");
        mStrings.add("C $30,000");
        mStrings.add("D $20,000");
        mStrings.add("E $10,000");
        mStrings.add("F $9,000");
        mStrings.add("G $8,000");
        mStrings.add("H $7,000");
        mStrings.add("I $6,000");
        mStrings.add("J $5,000");
        mStrings.add("K $4,000");
        mStrings.add("L $3,000");
        mStrings.add("M $2,000");
        mStrings.add("N $1,000");
        mStrings.add("O $9,00");
        mStrings.add("P $8,00");
        mStrings.add("Q $7,00");
        mStrings.add("R $6,00");
        mStrings.add("S $5,00");
        mStrings.add("T $4,00");
        mStrings.add("U $3,00");
        mStrings.add("V $2,00");
        mStrings.add("W $1,00");
        mStrings.add("X $50");
        mStrings.add("Y $25");
        mStrings.add("Z $10");
        /*
        Gson gson = new Gson();
        Log.e("jsone",gson.toJson(mStrings)) ;
        String js="[\"A $50,000\",\"B $40,000\",\"C $30,000\",\"D $20,000\",\"E $10,000\",\"F $9,000\",\"G $8,000\",\"H $7,000\",\"I $6,000\",\"J $5,000\",\"K $4,000\",\"L $3,000\",\"M $2,000\",\"N $1,000\",\"O $9,00\",\"P $8,00\",\"Q $7,00\",\"R $6,00\",\"S $5,00\",\"T $4,00\",\"U $3,00\",\"V $2,00\",\"W $1,00\",\"X $50\",\"Y $25\",\"Z $10\"]";
     ArrayList<String> c = gson.fromJson(js,ArrayList.class);
     Log.e("fromobj",c.get(6));
     */
    }

    @Override
    public void onValidationSucceeded() {
       // startActivity(new Intent(RegisterActivity.this, HomeActivity.class));

        HashMap<String, String> meMap=new HashMap<String, String>();
        meMap.put("email",registerEmail.getText().toString());
        meMap.put("full_name","k");

        meMap.put("password",regPassword.getText().toString());
        meMap.put("level","1");
        HandelCalls.getInstance(RegisterActivity.this).call(DataEnum.registerCall.name(),meMap,true,this);

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        Profile profile= (Profile) o;
        if(profile.getStatus().getStatus()){
            PrefsUtil.with(this).add(DataEnum.shProfile.name(),profile.getData()).apply();
            PrefsUtil.with(this).add(DataEnum.AuthToken.name(),profile.getData().getToken()).apply();
           // Data s= (Data) PrefsUtil.with(this).get(DataEnum.shProfile.name(),Data.class);
           // Log.e("my nmae",profile.getData().getFull_name());
            startActivity(new Intent(RegisterActivity.this, HomeActivity.class));
        }else{
            TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
        }
    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }
/*
    private void initSpinierLib() {
        mSimpleArrayListAdapter = new SimpleListAdapter(this, mStrings);
        mSearchableSpinner = (SearchableSpinner) findViewById(R.id.SearchableSpinner);
        mSearchableSpinner.setAdapter(mSimpleArrayListAdapter);
        mSearchableSpinner.setOnItemSelectedListener(mOnItemSelectedListener);
        mSearchableSpinner.setStatusListener(new IStatusListener() {
            @Override
            public void spinnerIsOpening() {

            }

            @Override
            public void spinnerIsClosing() {

            }
        });
    }

    private OnItemSelectedListener mOnItemSelectedListener = new OnItemSelectedListener() {
        @Override
        public void onItemSelected(View view, int position, long id) {
            Toast.makeText(RegisterActivity.this, "Item on position " + position + " : " + mSimpleArrayListAdapter.getItem(position) + " Selected", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onNothingSelected() {
            Toast.makeText(RegisterActivity.this, "Nothing Selected", Toast.LENGTH_SHORT).show();
        }
    };

*/


   /* private void initSpiner() {
        List<String> SpinnerList = new ArrayList<String>();
        for (int i = 0; i <10 ; i++) {
            SpinnerList.add(i+"mm");

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.spinner, SpinnerList);
        spLevel.setAdapter(adapter);

        spLevel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.e("item", "items");

                // initSelectedDocType(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }*/

}
