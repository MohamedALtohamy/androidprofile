package com.chatwithrich.mo.chatwithrich.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.chatwithrich.mo.chatwithrich.R;
import com.chatwithrich.mo.chatwithrich.adapter.AdapterUsers;
import com.chatwithrich.mo.chatwithrich.dialog.DialogInfoApp;
import com.chatwithrich.mo.chatwithrich.interfaces.HandleRetrofitResp;

import com.chatwithrich.mo.chatwithrich.model.profile.Profile;
import com.chatwithrich.mo.chatwithrich.model.users.Data;
import com.chatwithrich.mo.chatwithrich.model.users.ModelUsers;
import com.chatwithrich.mo.chatwithrich.retrofitconfig.HandelCalls;
import com.chatwithrich.mo.chatwithrich.utlitites.DataEnum;
import com.chatwithrich.mo.chatwithrich.utlitites.PrefsUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.malinskiy.superrecyclerview.OnMoreListener;
import com.malinskiy.superrecyclerview.SuperRecyclerView;
import com.sdsmdg.tastytoast.TastyToast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements OnMapReadyCallback, OnMoreListener, HandleRetrofitResp {

    private GoogleMap mMap;

    private LinearLayout linearMap, LinearList;
    private RecyclerView.LayoutManager mLayoutManager;
    private SuperRecyclerView mRecycler;
    private AdapterUsers mAdapter;
    private String nextPage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        initToolBar();
        initView();
        initMap();
        initClick();
        initRec();

        HandelCalls.getInstance(this).call(DataEnum.callAllProfiles.name(), new HashMap<String, String>(), true, this);


    }

    private void initRec() {


        mRecycler = (SuperRecyclerView) findViewById(R.id.list);
        mRecycler.hideProgress();
        mLayoutManager = new GridLayoutManager(this, 1);
        mRecycler.setLayoutManager(mLayoutManager);
        mRecycler.setupMoreListener(this, 1);

    }

    private void initMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void initToolBar() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        mToolbar.setTitle(getString(R.string.app_name));
        // getSupportActionBar().setTitle("Login");
    }

    private void initView() {
        linearMap = findViewById(R.id.linearMap);
        // LinearList = findViewById(R.id.LinearList);
    }

    public void updateSocilaAccount(HashMap<String, String> meMap) {
        if (meMap.size() > 0) {
            meMap.put("_method", "put");
        }
        HandelCalls.getInstance(this).call(DataEnum.updateProfile.name(), meMap, true, this);
    }

    private void initClick() {
        findViewById(R.id.imgSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // DialogInfoApp.showDialogSearch(HomeActivity.this);
            }
        });
        findViewById(R.id.tvList).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearMap.setVisibility(View.GONE);
                LinearList.setVisibility(View.VISIBLE);
            }
        });
        findViewById(R.id.tvMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                linearMap.setVisibility(View.VISIBLE);
                LinearList.setVisibility(View.GONE);
            }
        });
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void sendToStart() {

        Intent startIntent = new Intent(this, StartActivity.class);
        startActivity(startIntent);
        finish();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        if (item.getItemId() == R.id.main_logout_btn) {


            sendToStart();

        }

        if (item.getItemId() == R.id.main_settings_btn) {

            Intent settingsIntent = new Intent(this, UpdateProfileActivity.class);
            startActivity(settingsIntent);

        }

        if (item.getItemId() == R.id.main_all_btn) {
            DialogInfoApp.getInstance(HomeActivity.this).showDialogSocial(this);

            // Intent settingsIntent = new Intent(MainActivity.this, UsersActivity.class);
            //  startActivity(settingsIntent);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMoreAsked(int overallItemsCount, int itemsBeforeMore, int maxLastVisiblePosition) {

        if (nextPage != null) {
            //Log.e("next",nextPage);
            HashMap<String, String> meMap = new HashMap<String, String>();
            meMap.put(DataEnum.call_get_profile_next_page.name(), nextPage);

            HandelCalls.getInstance(this).call(DataEnum.call_get_profile_next_page.name(), meMap, false, this);

            //  HashMap<String, String> meMap = new HashMap<String, String>();
            // meMap.put(Constant.apiKey, Constant.apiValue);


        } else {
            mRecycler.hideMoreProgress();
        }

    }

    @Override
    public void onResponseSuccess(String flag, Object o) {
        //===========call get all users====================
        Log.e("respnse", "sucess");
        if (flag.equals(DataEnum.callAllProfiles.name())) {
            Log.e("respnse", DataEnum.callAllProfiles.name());
            ModelUsers modelUsers = (ModelUsers) o;
            if (modelUsers.getStatus().getStatus()) {
                Log.e("respnse", DataEnum.callAllProfiles.name());
                nextPage = modelUsers.getPagination().getNext_page_url();

                mAdapter = new AdapterUsers(modelUsers.getData(), HomeActivity.this);
                mRecycler.setAdapter(mAdapter);
            } else {
                List<Data> v = new ArrayList<>();
                mAdapter = new AdapterUsers(v, HomeActivity.this);
                mRecycler.setAdapter(mAdapter);
            }

        }
        //=========end cal get all users===================================//
        else if (flag.equals(DataEnum.call_get_profile_next_page.name())) {
            ModelUsers modelUsers = (ModelUsers) o;
            nextPage = modelUsers.getPagination().getNext_page_url();
            mAdapter.addAll(modelUsers.getData());
        }
//======================================update profile===========//
        else if (flag.equals(DataEnum.updateProfile.name())) {
            Profile profile = (Profile) o;
            if (profile.getStatus().getStatus()) {
                PrefsUtil.with(this).add(DataEnum.shProfile.name(), profile.getData()).apply();

            } else {
                TastyToast.makeText(this, profile.getStatus().getMessage(), TastyToast.LENGTH_LONG, TastyToast.ERROR);
            }
        }
    }

    @Override
    public void onNoContent(String flag, int code) {

    }

    @Override
    public void onResponseSuccess(String flag, Object o, int position) {

    }

    @Override
    public void onBadRequest(String flag, Object o) {

    }
/*
    @OnClick({R.id.imgFB, R.id.imgWhats})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgFB:
                SocialChat.FB(HomeActivity.this, "mohamed.algazzar.716");
                break;
            case R.id.imgWhats:
                SocialChat.email(HomeActivity.this, "eng@gmail.com");
                break;

        }

    }
*/
}
